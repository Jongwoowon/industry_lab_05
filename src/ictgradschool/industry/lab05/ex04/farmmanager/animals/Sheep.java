package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by jwon117 on 27/03/2017.
 */
public class Sheep implements Animal {

    /** All cow instances will have the same, shared, name: "Cow" */
    private static final String name = "Sheep";

    private int value;

	public Sheep() {
	    value = 500;
    }

    public void feed() {
        if (value < 1000) {
            value += (1000 - value)/2;
        }
    }

    public int costToFeed() {
        return 50;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value;
    }
}
