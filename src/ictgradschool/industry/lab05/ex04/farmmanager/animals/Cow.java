package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public class Cow implements Animal {

	/** All cow instances will have the same, shared, name: "Cow" */
	private static final String name = "Cow";

	private int value;
	
	public Cow() {
		value = 1000;
	}

	public void feed() {
		if (value < 1500) {
			value += 100;
		}
	}

	public int costToFeed() {
		return 60;
	}

	public String getName() {
		return name;
	}
	
	public int getValue() {
		return value;
	}

	public String toString() {
		return name + " - $" + value;
	}
}
