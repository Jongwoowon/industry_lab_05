package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by jwon117 on 27/03/2017.
 */
public class Chicken implements Animal {

    /** All cow instances will have the same, shared, name: "Cow" */
    private static final String name = "Chicken";

    private int value;

    public Chicken() {
        value = 200;
    }

    public void feed() {
        if (value < 400) {
            value += 50;
        }
    }

    public int costToFeed() {
        return 10;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value;
    }
}

