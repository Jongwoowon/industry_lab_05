package ictgradschool.industry.lab05.ex02;

/**
 * Represents something famous
 */
public interface IFamous {

    /**
     * Gets the name of a famous member of the species.
     */
    // What is a famous name for this animal
    public String famous();
}
