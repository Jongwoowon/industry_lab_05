package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.

        for (int i = 0; i < list.length; i++) {
            String mammalString = "";
            if (list[i].isMammal()) {
                mammalString += "mammal.";
            } else {
                mammalString += "non-mammal.";
            }

            System.out.println(list[i].myName() + " the " + list[i].getClass().getSimpleName() + " says " + list[i].sayHello());
            System.out.println(list[i].myName() + " the " + list[i].getClass().getSimpleName() + " is a " + mammalString);
            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs.");

            if (list[i] instanceof IFamous){
                IFamous famousAnimal = (IFamous) list[i];
                System.out.println("This is a famous name of my animal type: " + famousAnimal.famous());
            }

            System.out.println("--------------------------------------------------------------");
        }
    }


    // TODO If the animal also implements IFamous, print out that corresponding info too.

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
